package com.nguyenrepo.notifyme.Activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.nguyenrepo.notifyme.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
