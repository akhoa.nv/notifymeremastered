package com.nguyenrepo.notifyme.Activities;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.nguyenrepo.notifyme.Java.FirebaseService;
import com.nguyenrepo.notifyme.Java.NotificationService;
import com.nguyenrepo.notifyme.Java.Reservation;
import com.nguyenrepo.notifyme.R;

import net.glxn.qrgen.android.QRCode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class MainGuestActivity extends AppCompatActivity implements OnMapReadyCallback {
    private TextView m_text_host_name;
    private TextView m_text_host_phone;
    private TextView m_text_ticket_number;
    private TextView m_text_result;
    private TextView m_text_wait_time;
    private TextView m_text_ticket_name;
    private Button m_guest_ticket_cancel;

    private ConstraintLayout m_guest_ticket_detail;
    private ConstraintLayout m_guest_ticket_qr;

    private ProgressBar m_estimate_wait;

    private LinearLayout m_qr_pane;
    private ConstraintLayout m_reservation_pane;

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private MarkerOptions hostLocation;
    private Geocoder geocoder;

    private Bitmap qrBitmap;

    private boolean isTicketVisible = false;
    private Reservation mCurrentReservation = null;

    final String TAG = "MainGuestActivity";
    private SharedPreferences localUserSharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_guest);

        FirebaseService.init();
        localUserSharedPref = getSharedPreferences("local_user", Context.MODE_PRIVATE);

        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        m_text_host_name = (TextView) findViewById(R.id.text_host_name);
        m_text_host_phone = (TextView) findViewById(R.id.text_host_phone);
        m_text_ticket_number = (TextView) findViewById(R.id.text_ticket_number);
        m_text_result = (TextView) findViewById(R.id.text_result);
        m_text_wait_time = (TextView) findViewById(R.id.text_wait_time);
        m_text_ticket_name = (TextView) findViewById(R.id.text_ticket_name);
        m_guest_ticket_cancel = findViewById(R.id.guest_ticket_cancel);

        m_guest_ticket_detail = (ConstraintLayout) findViewById(R.id.guest_ticket_detail);
        m_guest_ticket_qr = (ConstraintLayout) findViewById(R.id.guest_ticket_qr);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        geocoder = new Geocoder(getApplicationContext());

        //Use QR
        qrBitmap = QRCode.from(currentUser.getUid()).withSize(256, 256).withColor(0xFF000000, 0x00FFFFFF).bitmap();
        final ImageView myImage = (ImageView) findViewById(R.id.image_qrcode);
        myImage.setImageBitmap(qrBitmap);

        // Map fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.reservation_map_view);
        mapFragment.getMapAsync(this);

        // Username
        m_text_ticket_name.setText(localUserSharedPref.getString("username", ""));

        //Initialize Notification Channel for Android 8.0 or above
        NotificationService.init(this);

        //Setup ValueListener for reservations
        final String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseService.watchUserTicket(userId, new FirebaseService.CallbackInterface() {
            @Override
            public void callback(DataSnapshot dataSnapshot) {
                String status = (String) dataSnapshot.child("status").getValue();
                if (status == null) {
                    m_text_host_name.setText("Not Reserved");
                    m_text_host_phone.setText("Not Reserved");
                    m_text_ticket_number.setText("Not Reserved");
                    m_text_result.setText("Not Reserved");
                    m_text_wait_time.setText("Not Reserved");
                    NotificationService.clearNotification(MainGuestActivity.this);
                    mCurrentReservation = null;
                    m_guest_ticket_cancel.setVisibility(View.INVISIBLE);

                    if (mMap != null){
                        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                                PackageManager.PERMISSION_GRANTED &&
                                ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                                        PackageManager.PERMISSION_GRANTED) {
                            getUserLocation();
                        }
                    }

                    return;
                }
                if (status.equalsIgnoreCase("not_ready")){
                    Reservation reservation = dataSnapshot.getValue(Reservation.class);
                    mCurrentReservation = reservation;
                    m_text_host_name.setText(reservation.getHostName());
                    m_text_host_phone.setText(reservation.getHostPhone());
                    m_text_ticket_number.setText(Long.toString(reservation.getTicketNumber()));
                    m_text_result.setText("Reserved");
                    m_text_wait_time.setText("5 mins");
                    NotificationService.notifyNewReservation(MainGuestActivity.this, reservation);
                    m_guest_ticket_cancel.setVisibility(View.VISIBLE);

                    List<Address> addresses = null;
                    try {
                        addresses = geocoder.getFromLocationName(reservation.getHostAddress(), 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if(addresses != null && addresses.size() > 0) {
                        double latitude= addresses.get(0).getLatitude();
                        double longitude= addresses.get(0).getLongitude();
                        markLocation(latitude, longitude, reservation.getHostName());
                    }
                }

                else if (status.equalsIgnoreCase("ready")){
                    Reservation reservation = dataSnapshot.getValue(Reservation.class);
                    mCurrentReservation = reservation;
                    m_text_result.setText("Ready");
                    NotificationService.notifyReservationReady(MainGuestActivity.this, reservation);
                    m_guest_ticket_cancel.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FirebaseService.destroyAllListeners();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 0:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                    getUserLocation();
                } else {
                    NotificationService.toastPermissionDenied(this, "Location");
                }
                return;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION }, 0);
        }

        mMap.setMinZoomPreference(15);

        getUserLocation();
    }

    private void getUserLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            //mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude()))
                                    //.title("You"));
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()),15));
                        }
                    }
                });
    }

    private void markLocation (double lat, double lng, String locationName) {
        if (mMap != null) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(locationName));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng),15));
        }
    }

    public void toggleTicket (View view) {
        if (!isTicketVisible) {
            m_guest_ticket_detail.setVisibility(View.VISIBLE);
            m_guest_ticket_qr.setVisibility(View.VISIBLE);

            m_guest_ticket_detail.animate().alpha(1f).setDuration(500);
            m_guest_ticket_qr.animate().alpha(1f).setDuration(500);
            isTicketVisible = true;
        } else {
            m_guest_ticket_detail.setVisibility(View.INVISIBLE);
            m_guest_ticket_qr.setVisibility(View.INVISIBLE);

            m_guest_ticket_detail.animate().alpha(0f).setDuration(500);
            m_guest_ticket_qr.animate().alpha(0f).setDuration(500);
            isTicketVisible = false;
        }
    }

    public void shareQR(View view) {
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpeg");
        ByteArrayOutputStream jpegTemp = new ByteArrayOutputStream();

        qrBitmap.compress(Bitmap.CompressFormat.JPEG, 100, jpegTemp);
        ContextWrapper cw = new ContextWrapper(getApplicationContext());

        File f = new File(cw.getDir("imageDir", Context.MODE_PRIVATE) + "qr.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(jpegTemp.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
        startActivity(Intent.createChooser(shareIntent, "Share QR using"));
    }

    public void openSetting(View view) {
        startActivityForResult(new Intent(MainGuestActivity.this, SettingActivity.class), 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == 1) {
                startActivity(new Intent(this, SignInActivity.class));
                this.finish();
            }
        }
    }

    public void onClick_delete(View view) {
        if (mCurrentReservation != null) {
            String restaurantId = mCurrentReservation.getHostId();
            String ticketNumber = Long.toString(mCurrentReservation.getTicketNumber());
            String userId = mCurrentReservation.getGuestId();
            FirebaseService.deleteReservation(restaurantId, ticketNumber, userId);
        }
    }
}
