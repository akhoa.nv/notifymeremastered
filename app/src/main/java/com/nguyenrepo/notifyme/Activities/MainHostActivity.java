package com.nguyenrepo.notifyme.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.firebase.database.DataSnapshot;
import com.nguyenrepo.notifyme.Java.ActiveReservationSubscriber;
import com.nguyenrepo.notifyme.Java.FirebaseService;
import com.nguyenrepo.notifyme.Java.NotificationService;
import com.nguyenrepo.notifyme.Java.Reservation;
import com.nguyenrepo.notifyme.Java.ReservationServiceHost;
import com.nguyenrepo.notifyme.Java.ReservationSlideAdapter;
import com.nguyenrepo.notifyme.R;

import java.util.List;

public class MainHostActivity extends AppCompatActivity implements ActiveReservationSubscriber {
    ReservationServiceHost reservationServiceHost = ReservationServiceHost.getInstance();
    final String[] m_currentGuestUserId = new String[1];
    final String[] m_currentGuestInfo = new String[3];
    List<Reservation> m_activeReservations = reservationServiceHost.getReservationList();

    private ViewPager viewpager;
    private ReservationSlideAdapter adapter;

    Button m_reservation_btn;
    Button m_reservation_cancel;

    LinearLayout m_host_camera;
    ImageButton m_qr_scan_btn;
    SeekBar m_page_seek_bar;
    TextView m_reservation_page;
    ImageButton m_reservation_setting;

    TextView m_reservation_scan_name;
    TextView m_reservation_scan_phone;
    TextView m_reservation_scan_email;
    TextView m_reservation_scan_header_name;
    TextView m_reservation_scan_header_email;
    TextView m_reservation_scan_header_phone;
    
    boolean isCameraOn = false;
    boolean isCheckInMode = false;

    final int REQUEST_CAMERA_PERMISSION_ID = 1001;
    final String TAG = "MainHostActivity";

    @Override
    protected void onDestroy() {
        ReservationServiceHost.unsubscribe(this);
        FirebaseService.destroyAllListeners();
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Default codes
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_host);

        FirebaseService.init();
        //Find UI Views
        m_reservation_btn = (Button) findViewById(R.id.reservation_btn);
        m_reservation_cancel = (Button) findViewById(R.id.reservation_cancel);

        m_host_camera = (LinearLayout) findViewById(R.id.host_camera);
        m_qr_scan_btn = (ImageButton) findViewById(R.id.reservation_qr_scan);
        m_page_seek_bar = (SeekBar) findViewById(R.id.reservation_scroll);
        m_reservation_page = (TextView) findViewById(R.id.reservation_page);
        m_reservation_setting = (ImageButton) findViewById(R.id.reservation_setting_btn);
        m_reservation_scan_name = (TextView) findViewById(R.id.reservation_scan_name);
        m_reservation_scan_phone = (TextView) findViewById(R.id.reservation_scan_phone);
        m_reservation_scan_email = (TextView) findViewById(R.id.reservation_scan_email);
        m_reservation_scan_header_name = (TextView) findViewById(R.id.reservation_scan_header_name);
        m_reservation_scan_header_phone = (TextView) findViewById(R.id.reservation_scan_header_phone);
        m_reservation_scan_header_email = (TextView) findViewById(R.id.reservation_scan_header_email);

        ReservationServiceHost.subscribe(this);

        // Initialization of view
        viewpager = (ViewPager) findViewById(R.id.reservation_pager);
        adapter = new ReservationSlideAdapter(this, findViewById(R.id.host_main_view),
                new ReservationSlideAdapter.MyAdapterCallback() {
            @Override
            public void checkIn() {
                isCheckInMode = true;
                onClick_startQRScan(m_qr_scan_btn);
            }
        });
        viewpager.setAdapter(adapter);

        m_page_seek_bar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        m_page_seek_bar.getThumb().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        m_page_seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    adapter.notifyDataSetChanged();
                    viewpager.setCurrentItem(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        ViewPager.OnPageChangeListener pagerListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                if (position % 2 == 0) {
                    m_qr_scan_btn.setImageResource(R.drawable.qr_icon_black);
                    m_qr_scan_btn.setBackground(getResources().getDrawable(R.drawable.round_white));
                    m_reservation_btn.setTextColor(Color.BLACK);
                    m_reservation_cancel.setTextColor(Color.BLACK);
                    m_page_seek_bar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                    m_page_seek_bar.getThumb().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                    m_reservation_page.setTextColor(Color.WHITE);
                    m_reservation_setting.setImageResource(R.drawable.setting_icon_white);

                    m_reservation_scan_name.setTextColor(Color.BLACK);
                    m_reservation_scan_phone.setTextColor(Color.BLACK);
                    m_reservation_scan_email.setTextColor(Color.BLACK);
                    m_reservation_scan_header_name.setTextColor(Color.BLACK);
                    m_reservation_scan_header_phone.setTextColor(Color.BLACK);
                    m_reservation_scan_header_email.setTextColor(Color.BLACK);
                    m_host_camera.setBackground(getResources().getDrawable(R.drawable.ticket));
                    m_reservation_btn.setBackground(getResources().getDrawable(R.drawable.half_corner_button_bottom_left_white));
                    m_reservation_cancel.setBackground(getResources().getDrawable(R.drawable.half_corner_button_bottom_right_white));
                } else {
                    m_qr_scan_btn.setImageResource(R.drawable.qr_icon_white);
                    m_qr_scan_btn.setBackground(getResources().getDrawable(R.drawable.round_black));
                    m_reservation_btn.setTextColor(Color.WHITE);
                    m_reservation_cancel.setTextColor(Color.WHITE);
                    m_page_seek_bar.getProgressDrawable().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
                    m_page_seek_bar.getThumb().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
                    m_reservation_page.setTextColor(Color.BLACK);
                    m_reservation_setting.setImageResource(R.drawable.setting_icon_black);

                    m_reservation_scan_name.setTextColor(Color.WHITE);
                    m_reservation_scan_phone.setTextColor(Color.WHITE);
                    m_reservation_scan_email.setTextColor(Color.WHITE);
                    m_reservation_scan_header_name.setTextColor(Color.WHITE);
                    m_reservation_scan_header_phone.setTextColor(Color.WHITE);
                    m_reservation_scan_header_email.setTextColor(Color.WHITE);
                    m_host_camera.setBackground(getResources().getDrawable(R.drawable.ticket_black));
                    m_reservation_btn.setBackground(getResources().getDrawable(R.drawable.half_corner_button_bottom_left_black));
                    m_reservation_cancel.setBackground(getResources().getDrawable(R.drawable.half_corner_button_bottom_right_black));
                }

                m_page_seek_bar.setProgress(position);
                m_reservation_page.setText((position + 1) + "/" + adapter.getCount());
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        };

        viewpager.addOnPageChangeListener(pagerListener);

        //Setting up barcode detector
    }

    public void onClick_makeReservation(View view) {
        if (m_currentGuestUserId[0] == null) {
            return;
        }
        reservationServiceHost.makeReservation(m_currentGuestInfo[0], m_currentGuestInfo[1],
                m_currentGuestInfo[2], m_currentGuestUserId[0], m_reservation_btn);

        NotificationService.toastReservationSuccess(this);
        m_host_camera.setVisibility(View.INVISIBLE);
    }

    public void onClick_startQRScan(View view){
        Intent intent = new Intent(this, CameraActivity.class);
        startActivityForResult(intent, 1);
    }

    public void updateActiveReservation(List<Reservation> list) {
        m_activeReservations = list;

        if (m_activeReservations.size() == 0) {
            m_page_seek_bar.setVisibility(View.INVISIBLE);
            m_reservation_page.setVisibility(View.INVISIBLE);

            m_qr_scan_btn.setImageResource(R.drawable.qr_icon_black);
            m_qr_scan_btn.setBackground(getResources().getDrawable(R.drawable.round_white));
            m_page_seek_bar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
            m_page_seek_bar.getThumb().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
            m_reservation_page.setTextColor(Color.WHITE);
        } else {
            if (m_activeReservations.size() != 1) {
                m_page_seek_bar.setMax(m_activeReservations.size() - 1);
                m_page_seek_bar.setVisibility(View.VISIBLE);
                m_reservation_page.setVisibility(View.VISIBLE);
            } else if (m_activeReservations.size() == 1) {
                m_page_seek_bar.setVisibility(View.INVISIBLE);
                m_reservation_page.setVisibility(View.INVISIBLE);
            }

            m_page_seek_bar.setProgress(viewpager.getCurrentItem());
            m_reservation_page.setText((viewpager.getCurrentItem() + 1) + "/" + adapter.getCount());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                m_currentGuestUserId[0] = data.getStringExtra("result");
                if (isCheckInMode) {
                    FirebaseService.readUserTicket(m_currentGuestUserId[0], new FirebaseService.CallbackInterface() {
                        @Override
                        public void callback(DataSnapshot dataSnapshot) {
                            Reservation reservation = dataSnapshot.getValue(Reservation.class);
                            if (reservation == null)
                                return;
                            if (reservation.getStatus() == null)
                                return;
                            if (reservation.getStatus().equalsIgnoreCase("ready")) {
                                reservationServiceHost.delete(reservation.getGuestId());
                                isCheckInMode = false;
                                NotificationService.toastTicketCompleted(
                                        MainHostActivity.this, reservation);
                            }
                        }
                    });
                } else {
                    FirebaseService.readUserInfo(m_currentGuestUserId[0], new FirebaseService.CallbackInterface() {
                        @Override
                        public void callback(DataSnapshot dataSnapshot) {
                            //Retrieve customer's information
                            m_currentGuestInfo[0] = (String) dataSnapshot.child("name").getValue();
                            m_currentGuestInfo[1] = (String) dataSnapshot.child("email").getValue();
                            m_currentGuestInfo[2] = (String) dataSnapshot.child("phoneNumber").getValue();
                            m_host_camera.setVisibility(View.VISIBLE);

                            m_reservation_scan_name.setText(m_currentGuestInfo[0]);
                            m_reservation_scan_phone.setText(m_currentGuestInfo[2]);
                            m_reservation_scan_email.setText(m_currentGuestInfo[1]);
                        }
                    });
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //TODO: Deal with Camera Being cancelled
                isCheckInMode = false;
            }
        } else if (requestCode == 0) {
            if (resultCode == 1) {
                startActivity(new Intent(this, SignInActivity.class));
                this.finish();
            }
        }
    }

    public void openSetting(View view) {
        startActivityForResult(new Intent(MainHostActivity.this, SettingActivity.class), 0);
    }

    public void cancelScanner (View view) {
        m_host_camera.setVisibility(View.INVISIBLE);
    }
}


