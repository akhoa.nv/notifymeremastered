package com.nguyenrepo.notifyme.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nguyenrepo.notifyme.Java.AuthenticationService;
import com.nguyenrepo.notifyme.Java.FirebaseService;
import com.nguyenrepo.notifyme.Java.SettingAdapter;
import com.nguyenrepo.notifyme.R;

import java.util.ArrayList;

public class SettingActivity extends AppCompatActivity implements SettingAdapter.ItemClickListener {
    private SettingAdapter adapter;
    private ArrayList<String> header = new ArrayList<>();
    private ArrayList<Integer> icon = new ArrayList<>();

    private RecyclerView m_recycler_view;
    private TextView m_username;
    private TextView m_email;
    private TextView m_phone;
    private ImageView m_user_img;

    private static final String TAG = "SettingActivity";
    private static SharedPreferences localUserSharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        FirebaseService.init();
        localUserSharedPref = getSharedPreferences("local_user", Context.MODE_PRIVATE);

        m_username = findViewById(R.id.setting_username);
        m_email = findViewById(R.id.setting_email);
        m_phone = findViewById(R.id.setting_phone);
        m_user_img = findViewById(R.id.setting_user_img);

        m_username.setText(localUserSharedPref.getString("username", ""));
        m_email.setText(localUserSharedPref.getString("email", ""));
        m_phone.setText(PhoneNumberUtils.formatNumber(localUserSharedPref.getString("phone", "")));

        if (localUserSharedPref.getString("type", "").equals("host")) {
            m_user_img.setImageResource(R.drawable.host);
        } else if (localUserSharedPref.getString("type", "").equals("guest")) {
            m_user_img.setImageResource(R.drawable.client);
        }

        header.add("Report a Problem");
        header.add("Legal & Policies");
        header.add("Sign Out");

        icon.add(R.drawable.bug_icon);
        icon.add(R.drawable.legal_icon);
        icon.add(R.drawable.signout_icon);

        m_recycler_view = findViewById(R.id.setting_recycle_view);
        m_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SettingAdapter(this, header, icon);
        adapter.setClickListener(this);
        m_recycler_view.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        if (position == adapter.getItemCount() - 1){
            setResult(1, null);
            AuthenticationService.signOut(this);
        }
    }
}
