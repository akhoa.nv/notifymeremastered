package com.nguyenrepo.notifyme.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.nguyenrepo.notifyme.Java.AuthenticationService;
import com.nguyenrepo.notifyme.Java.AuthenticationSubscriber;
import com.nguyenrepo.notifyme.Java.FirebaseService;
import com.nguyenrepo.notifyme.R;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener, AuthenticationSubscriber {
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;
    private static Context context;
    private static SharedPreferences localUserSharedPref;

    ImageView m_sign_in_logo;
    TextView m_tos_header;
    Button m_sign_in_btn;
    LinearLayout m_sign_in_tos_container;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private static GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        localUserSharedPref = getSharedPreferences("local_user", Context.MODE_PRIVATE);

        m_sign_in_logo = findViewById(R.id.signin_logo);
        m_sign_in_btn = findViewById(R.id.signin_btn);
        m_tos_header = findViewById(R.id.signin_tos_header);
        m_sign_in_tos_container = findViewById(R.id.signin_tos_container);

        FirebaseService.init();
        // Button listeners
        findViewById(R.id.signin_btn).setOnClickListener(this);

        AuthenticationService.getInstance();
        AuthenticationService.init(this);
        AuthenticationService.subscribe(this);

        mGoogleSignInClient = AuthenticationService.getGoogleSignInClient();

        // [START initialize_auth]
        // Initialize Firebase Auth
        mAuth = AuthenticationService.getFirebaseAuth();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        //If the user already logged in
        if (currentUser != null) {
            m_sign_in_btn.setVisibility(View.INVISIBLE);
            m_tos_header.setVisibility(View.INVISIBLE);
            m_sign_in_tos_container.setVisibility(View.INVISIBLE);

            m_sign_in_btn.setAlpha(0f);
            m_tos_header.setAlpha(0f);
            m_sign_in_tos_container.setAlpha(0f);

            m_sign_in_logo.clearAnimation();

            enterApp(currentUser);
        } else {
            m_sign_in_logo.animate().translationYBy(-250).setDuration(500);

            m_sign_in_btn.setVisibility(View.VISIBLE);
            m_tos_header.setVisibility(View.VISIBLE);
            m_sign_in_tos_container.setVisibility(View.VISIBLE);

            m_sign_in_btn.animate().alpha(1f).setDuration(700);
            m_tos_header.animate().alpha(1f).setDuration(700);
            m_sign_in_tos_container.animate().alpha(1f).setDuration(700);
        }
        // [END initialize_auth]

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        context = getApplicationContext();
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }
    // [END on_start_check_user]

    // [START onactivityresult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // [START_EXCLUDE]
                updateUI(null);
                // [END_EXCLUDE]
            }
        } else if (requestCode == 0) {
            if (resultCode == 2) {
                this.finish();
            }
        }
    }
    // [END onactivityresult]

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());


        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            enterApp(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                    }
                });
    }
    // [END auth_with_google]

    // [START signin]
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signin]

    private void signOut() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(null);
                    }
                });
    }

    public static GoogleSignInClient getGoogleSignInClient() {
        return mGoogleSignInClient;
    }

    private void revokeAccess() {
        // Firebase sign out
        mAuth.signOut();

        // Google revoke access
        mGoogleSignInClient.revokeAccess().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(null);
                    }
                });
    }

    @SuppressLint("StringFormatInvalid")
    private void updateUI(FirebaseUser user) {
        if (user != null) {
            findViewById(R.id.signin_btn).setVisibility(View.GONE);
        } else {
            findViewById(R.id.signin_btn).setVisibility(View.VISIBLE);
        }
    }

    private void enterApp(FirebaseUser user) {
        //Check if the user is already registered
        final String userId = user.getUid();

        FirebaseService.init();
        FirebaseService.readUserInfo(userId, new FirebaseService.CallbackInterface() {
            @Override
            public void callback(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    Log.d(TAG, "User exists!");
                    String type = (String) dataSnapshot.child("type").getValue();

                    if (localUserSharedPref.getString("Username", "").equals("")){
                        AuthenticationService.storeLocalUserVar(SignInActivity.this);
                    }
                }
                else {
                    Log.d(TAG, "User doesn't exist");
                    startActivityForResult(new Intent(SignInActivity.this, SignUpActivity.class), 0);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.signin_btn) {
            signIn();
        }
    }

    public static Context getContext(){
        return context;
    }

    public void authenticationFinished(String type) {
        AuthenticationService.unsubscribe(this);
        if (type.equalsIgnoreCase("Host")) {
            startActivity(new Intent(SignInActivity.this, MainHostActivity.class));
            finish();
        } else {
            startActivity(new Intent(SignInActivity.this, MainGuestActivity.class));
            finish();
        }
    }
}
