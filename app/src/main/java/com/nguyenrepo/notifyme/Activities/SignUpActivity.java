package com.nguyenrepo.notifyme.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.nguyenrepo.notifyme.Java.AuthenticationService;
import com.nguyenrepo.notifyme.Java.FirebaseService;
import com.nguyenrepo.notifyme.Java.SignInSlideAdapter;
import com.nguyenrepo.notifyme.R;

public class SignUpActivity extends AppCompatActivity {
    private ViewPager viewpager;
    private SignInSlideAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        viewpager = (ViewPager) findViewById(R.id.signup_pager);
        adapter = new SignInSlideAdapter(this);
        viewpager.setAdapter(adapter);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                setResult(2, null);
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        AuthenticationService.signOut(this);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FirebaseService.destroyAllListeners();
    }
}
