package com.nguyenrepo.notifyme.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nguyenrepo.notifyme.Java.AuthenticationService;
import com.nguyenrepo.notifyme.Java.FirebaseService;
import com.nguyenrepo.notifyme.Java.Guest;
import com.nguyenrepo.notifyme.R;

public class SignUpGuestActivity extends AppCompatActivity {
    private EditText m_field_name;
    private EditText m_field_email;
    private EditText m_field_phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_guest);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseService.init();
        //Autofill name and email fields
        m_field_name = (EditText) findViewById(R.id.field_name);
        m_field_email = (EditText) findViewById(R.id.field_email);
        m_field_phoneNumber = (EditText) findViewById(R.id.field_phone);

        m_field_name.setText(currentUser.getDisplayName());
        m_field_email.setText(currentUser.getEmail());

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }
    public void onClick_Register(View view) {
        //Validate fields

        //Creating a Host object
        String name = m_field_name.getText().toString();
        String email = m_field_email.getText().toString();
        String phone = m_field_phoneNumber.getText().toString();

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        Guest newGuest = new Guest(name, email, phone);
        FirebaseService.writeGuestToFirebase(userId, newGuest);
        startActivity(new Intent(SignUpGuestActivity.this, MainGuestActivity.class));

        setResult(RESULT_OK, null);
        AuthenticationService.storeLocalUserVar(this);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FirebaseService.destroyAllListeners();
    }
}
