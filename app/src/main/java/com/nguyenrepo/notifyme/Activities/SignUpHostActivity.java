package com.nguyenrepo.notifyme.Activities;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.nguyenrepo.notifyme.Java.AuthenticationService;
import com.nguyenrepo.notifyme.Java.FirebaseService;
import com.nguyenrepo.notifyme.Java.Host;
import com.nguyenrepo.notifyme.Java.Restaurant;
import com.nguyenrepo.notifyme.R;

import java.util.Arrays;
import java.util.List;

public class SignUpHostActivity extends AppCompatActivity {
    private EditText m_field_name;
    private EditText m_field_email;
    private EditText m_field_phoneNumber;
    private EditText m_field_restaurantName;
    private EditText m_field_address;
    String TAG = "SignUpHostActivity";
    int AUTOCOMPLETE_REQUEST_CODE = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_host);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseService.init();
        //Autofill name and email fields
        m_field_name = (EditText) findViewById(R.id.field_name);
        m_field_email = (EditText) findViewById(R.id.field_email);
        m_field_phoneNumber = (EditText) findViewById(R.id.field_phone);
        m_field_restaurantName = (EditText) findViewById(R.id.field_restaurantName);
        m_field_address = (EditText) findViewById(R.id.field_address);

        m_field_name.setText(currentUser.getDisplayName());
        m_field_email.setText(currentUser.getEmail());

        ApplicationInfo ai = null;
        try {
            ai = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Bundle bundle = ai.metaData;
        String apiKey = bundle.getString("com.google.android.geo.API_KEY");
        Places.initialize(getApplicationContext(), apiKey);
        PlacesClient placesClient = Places.createClient(this);
    }

    public void onClick_Register(View view) {
        //Validate fields

        //Creating a Host object
        final String name = m_field_name.getText().toString();
        final String email = m_field_email.getText().toString();
        final String phone = m_field_phoneNumber.getText().toString();
        final String restaurantName = m_field_restaurantName.getText().toString();
        final String address = m_field_address.getText().toString();
        final String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        //Creating new Restaurant (Use Push to generate new ID)
        //Possible race condition. Did not used Transaction for simplicity but SHOULD
        FirebaseService.readRestaurantIdCounter(new FirebaseService.CallbackInterface() {
            @Override
            public void callback(DataSnapshot dataSnapshot) {
                Long restaurantId = (Long) dataSnapshot.getValue();
                FirebaseService.writeRestaurantIdCounter(new Long(restaurantId + 1));
                //Create Restaurant
                Restaurant newRestaurant = new Restaurant(restaurantName, address, phone);
                FirebaseService.writeRestaurantToFirebase(Long.toString(restaurantId), newRestaurant);
                //Tie restaurant ID to User
                //Register User
                Host newHost = new Host(name, email, phone, restaurantName, address,
                        Long.toString(restaurantId));
                FirebaseService.writeHostToFirebase(userId, newHost);
                FirebaseService.writeDefaultTicketCounter(Long.toString(restaurantId));
                Log.d(TAG, "User registration success!");
                startActivity(new Intent(SignUpHostActivity.this, MainHostActivity.class));

                setResult(RESULT_OK, null);
                AuthenticationService.storeLocalUserVar(SignUpHostActivity.this);
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FirebaseService.destroyAllListeners();
    }

    public void onClick_Autocomplete(View view) {

        List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS);

        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                m_field_address.setText(place.getAddress());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
