package com.nguyenrepo.notifyme.Java;

import java.util.List;

public interface ActiveReservationSubscriber {
    void updateActiveReservation(List<Reservation> l);
}
