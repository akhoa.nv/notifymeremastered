package com.nguyenrepo.notifyme.Java;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.nguyenrepo.notifyme.R;

import java.util.ArrayList;

public class AuthenticationService {
    private static AuthenticationService instance = null;
    private static Context context;
    private static SharedPreferences sharedPref;
    private static SharedPreferences.Editor sharedPrefEditor;
    private static ArrayList<AuthenticationSubscriber> subscribers = new ArrayList<>();

    private static GoogleSignInClient mGoogleSignInClient;
    private static FirebaseAuth mAuth;

    private static final String TAG = "AuthenticationService";

    public static void subscribe(AuthenticationSubscriber s) {
        subscribers.add(s);
    }

    public static void unsubscribe(AuthenticationSubscriber s) {
        subscribers.remove(s);
    }

    private static void update(String type) {
        for (int i = 0; i < subscribers.size(); i++) {
            subscribers.get(i).authenticationFinished(type);
        }
    }

    private AuthenticationService() {}

    public static AuthenticationService getInstance() {
        if (instance == null) {
            instance = new AuthenticationService();
        }

        return instance;
    }

    public static void init(Activity activity) {
        context = activity.getApplicationContext();

        FirebaseService.init();
        // [START config_signin]
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(context.getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // [END config_signin]

        mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);

        // [START initialize_auth]
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        sharedPref = activity.getSharedPreferences("local_user", Context.MODE_PRIVATE);
        sharedPrefEditor = sharedPref.edit();
    }

    public static void signOut(final Activity activity) {
        clearLocalUserVar(activity);

        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(activity,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        activity.finish();
                    }
                });
    }

    public static GoogleSignInClient getGoogleSignInClient() {
        return mGoogleSignInClient;
    }

    public static FirebaseAuth getFirebaseAuth() {
        return mAuth;
    }

    public static void storeLocalUserVar(final Activity activity) {
        String userId = FirebaseAuth.getInstance().getUid();
        FirebaseService.readUserInfo(userId, new FirebaseService.CallbackInterface() {
            @Override
            public void callback(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("type").getValue(String.class).equalsIgnoreCase("Host")) {
                    Host host = dataSnapshot.getValue(Host.class);
                    sharedPrefEditor.putString("username", host.getName());
                    sharedPrefEditor.putString("email", host.getEmail());
                    sharedPrefEditor.putString("phone", host.getPhoneNumber());
                    sharedPrefEditor.putString("type", "host");
                }
                else {
                    Guest guest = dataSnapshot.getValue(Guest.class);
                    sharedPrefEditor.putString("username", guest.getName());
                    sharedPrefEditor.putString("email", guest.getEmail());
                    sharedPrefEditor.putString("phone", guest.getPhoneNumber());
                    sharedPrefEditor.putString("type", "guest");
                }
                sharedPrefEditor.commit();

                update(dataSnapshot.child("type").getValue(String.class));
            }
        });
    }

    private static void clearLocalUserVar(Activity activity) {
        sharedPrefEditor.putString("username", "");
        sharedPrefEditor.putString("email", "");
        sharedPrefEditor.putString("phone", "");
        sharedPrefEditor.putString("type", "");
        sharedPrefEditor.commit();
    }
}
