package com.nguyenrepo.notifyme.Java;

import com.google.firebase.auth.FirebaseUser;

public interface AuthenticationSubscriber {
    void authenticationFinished(String type);
}
