package com.nguyenrepo.notifyme.Java;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class FirebaseService {
    private static DatabaseReference mRootRef;
    private volatile static Map<DatabaseReference, ValueEventListener> listeners = new HashMap<>();
    private volatile static Map<DatabaseReference, ValueEventListener> guestListeners = new HashMap<>();

    private static final String TAG = "FirebaseService";

    public static void init() {
        mRootRef = FirebaseDatabase.getInstance().getReference();
    }

    public static void readUserInfo(final String userId, final CallbackInterface callbackInterface) {
        DatabaseReference userRef = mRootRef.child("users").child(userId);
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (callbackInterface != null)
                    callbackInterface.callback(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "Failed to read user's information", databaseError.toException());
            }
        });
    }

    public static void writeGuestToFirebase(String userId, Guest guest) {
        mRootRef.child("users").child(userId).setValue(guest);
        //Put placeholder in DB so we can have a handle to guest_db
        mRootRef.child("guest_db").child(userId).child("0").setValue(0);
    }

    public static void writeHostToFirebase(String userId, Host host) {
        mRootRef.child("users").child(userId).setValue(host);
    }

    public static void readUserTicket(final String userId, final CallbackInterface callbackInterface) {
        mRootRef.child("guest_db").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (callbackInterface != null)
                    callbackInterface.callback(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "Failed to read reservation for user" + userId,
                        databaseError.toException());
            }
        });
    }

    public static void watchUserTicket(final String userId, final CallbackInterface callbackInterface) {
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (callbackInterface != null)
                    callbackInterface.callback(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "Failed to watch reservation for user" + userId,
                        databaseError.toException());
            }
        };
        mRootRef.child("guest_db").child(userId).addValueEventListener(listener);
        addListener(mRootRef.child("guest_db").child(userId), listener);
    }

    public static void deleteUserTicket(final String restaurantId, final String ticketNumber, final String userId) {
        mRootRef.child("guest_db").child(userId).removeValue();
        mRootRef.child("restaurant_db").child(restaurantId).child("active").child(ticketNumber).removeValue();
    }

    public static void readRestaurantIdCounter(final CallbackInterface callbackInterface) {
        mRootRef.child("counters").child("restaurantId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(callbackInterface != null)
                    callbackInterface.callback(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "Failed to read Restaurant ID Counter", databaseError.toException());
            }
        });
    }

    public static void writeRestaurantIdCounter(Long restaurantIdCounter) {
        mRootRef.child("counters").child("restaurantId").setValue(restaurantIdCounter);
    }

    public static void writeRestaurantToFirebase(String restaurantId, Restaurant restaurant) {
        final DatabaseReference restaurantRef = mRootRef.child("restaurants");
        restaurantRef.child(restaurantId).setValue(restaurant);
    }

    public static void readRestaurantTicketCounter(String restaurantId, final CallbackInterface callbackInterface) {
        mRootRef.child("restaurant_db").child(restaurantId).child("ticket_counter").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (callbackInterface != null) {
                    callbackInterface.callback(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "Failed to read Ticket Counter", databaseError.toException());
            }
        });
    }

    public static void watchRestaurantTicketCounter(String restaurantId, final CallbackInterface callbackInterface) {
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (callbackInterface != null) {
                    callbackInterface.callback(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "Failed to watch Ticket Counter", databaseError.toException());
            }
        };
        mRootRef.child("restaurant_db").child(restaurantId).addValueEventListener(listener);
        addListener(mRootRef.child("restaurant_db").child(restaurantId), listener);
    }

    public static void writeDefaultTicketCounter(String restaurantId) {
        mRootRef.child("restaurant_db").child(restaurantId).
                child("ticket_counter").setValue(new Long(0));
    }

    public static void writeTicketCounter(String restaurantId, Long ticketNumber) {
        mRootRef.child("restaurant_db").child(restaurantId).
                child("ticket_counter").setValue(ticketNumber);
    }

    public static void watchActiveReservations(String restaurantId, final CallbackInterface callbackInterface) {
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (callbackInterface != null) {
                    callbackInterface.callback(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "Failed to watch Host's Active Reservations", databaseError.toException());
            }
        };
        mRootRef.child("restaurant_db").child(restaurantId).addValueEventListener(listener);
        addListener(mRootRef.child("restaurant_db").child(restaurantId), listener);
    }

    public static void watchGuestReservation(final String guestId, final CallbackInterface callbackInterface) {
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    DatabaseReference reference = dataSnapshot.getRef();
                    reference.removeEventListener(this);
                }
                if (callbackInterface != null) {
                    callbackInterface.callback(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, String.format("Failed to watch Guest's Reservation (id %s)", guestId),
                        databaseError.toException());
            }
        };
        mRootRef.child("guest_db").child(guestId).addValueEventListener(listener);
        addGuestListner(mRootRef.child("guest_db").child(guestId), listener);
    }

    public static void writeReservationToFirebase(String restaurantId, String userId, Reservation reservation) {
        mRootRef.child("guest_db").child(userId).setValue(reservation);
        mRootRef.child("restaurant_db").child(restaurantId).child("active")
                .child(Long.toString(reservation.getTicketNumber())).setValue(reservation);
    }

    public static void writeReadyStatusToReservation(String restaurantId, String userId, Reservation reservation) {
        mRootRef.child("guest_db").child(userId).child("status").setValue("ready");
        mRootRef.child("restaurant_db").child(restaurantId).child("active")
                .child(Long.toString(reservation.getTicketNumber())).child("status").setValue("ready");
    }

    public static void deleteReservation(final String restaurantId, final String ticketNumber, final String userId) {
        deleteUserTicket(restaurantId, ticketNumber, userId);
    }

    public static void addListener(DatabaseReference reference, ValueEventListener listener) {
        if (listeners.get(reference) == null) {
            listeners.put(reference, listener);
        }
    }

    public static void addGuestListner(DatabaseReference reference, ValueEventListener listener) {
        if (guestListeners.get(reference) == null) {
            guestListeners.put(reference, listener);
        }
    }

    public static void destroyAllListeners() {
        for (Map.Entry<DatabaseReference, ValueEventListener> entry : listeners.entrySet()) {
            DatabaseReference reference = entry.getKey();
            ValueEventListener listener = entry.getValue();
            reference.removeEventListener(listener);
        }
        listeners.clear();
        destroyGuestListeners();
    }

    public static void destroyGuestListeners() {
        for (Map.Entry<DatabaseReference, ValueEventListener> entry : guestListeners.entrySet()) {
            DatabaseReference reference = entry.getKey();
            ValueEventListener listener = entry.getValue();
            reference.removeEventListener(listener);
        }
        guestListeners.clear();
    }

    public interface CallbackInterface {
        void callback(DataSnapshot dataSnapshot);
    }
}
