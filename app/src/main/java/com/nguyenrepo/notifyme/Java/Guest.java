package com.nguyenrepo.notifyme.Java;

public class Guest {
    private String name;
    private String email;
    private String phoneNumber;
    private String reservationId;
    private String type;

    public Guest() {

    }

    public Guest(String name, String email, String phoneNumber) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.reservationId = reservationId;
        this.type = "Guest";
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getReservationId() {
        return reservationId;
    }

    public String getType() {
        return type;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }
}
