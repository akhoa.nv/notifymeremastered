package com.nguyenrepo.notifyme.Java;

public class Host {
    private String name;
    private String email;
    private String phoneNumber;
    private String restaurantName;
    private String restaurantAddress;
    private String restaurantId;
    private String type;

    public Host() {

    }

    public Host(String name, String email, String phoneNumber, String restaurantName,
                String restaurantAddress, String restaurantId) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.restaurantName = restaurantName;
        this.restaurantAddress = restaurantAddress;
        this.restaurantId = restaurantId;
        this.type = "Host";
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public String getType() {
        return type;
    }
}
