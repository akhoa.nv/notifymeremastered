package com.nguyenrepo.notifyme.Java;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.nguyenrepo.notifyme.Activities.MainGuestActivity;
import com.nguyenrepo.notifyme.R;

public class NotificationService {

    private static final String CHANNEL_ID = "com.nguyenrepo.notifyme.notification.ANDROID";
    private static int PRIORITY_VALUE = NotificationCompat.PRIORITY_HIGH;
    private static CharSequence name;
    private static String description;
    private static Uri soundUri;
    private static int currentNotificationID = 0; // Allow only one notification at a time
    private static int toastDuration = Toast.LENGTH_LONG;
    private static String TAG = "NotificationService";

    public static void init(Context context) {
        // Only create NotificationChannel if API > 26
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            name = context.getString(R.string.channel_name);
            description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            // Register the channel with the system
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
        soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    }

    public static void notifyNewReservation(Context context, Reservation reservation) {
        String title = "Reservation at %s";
        String contentText = "Your ticket number is %s.\nClick for more detail";
        Intent intent = new Intent(context, MainGuestActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                // TODO: Added notification logo
                .setSmallIcon(R.drawable.notify_me)
                .setContentTitle(String.format(title, reservation.getHostName()))
                .setContentText(String.format(contentText, reservation.getTicketNumber()))
                .setDefaults(Notification.DEFAULT_ALL)
                //.setSound(soundUri)
                .setPriority(PRIORITY_VALUE)
                .setChannelId(CHANNEL_ID)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancelAll();
        currentNotificationID = NotificationID.getUniqueID();
        notificationManager.notify(currentNotificationID, builder.build());
        vibrate(context);
    }

    public static void notifyReservationReady(Context context, Reservation reservation) {
        String title = "Your table is ready!";
        String contentText = "Please head back to %s";
        Intent intent = new Intent(context, MainGuestActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                // TODO: Added notification logo
                .setSmallIcon(R.drawable.notify_me)
                .setContentTitle(title)
                .setContentText(String.format(contentText, reservation.getHostName()))
                .setDefaults(Notification.DEFAULT_ALL)
                //.setSound(soundUri)
                .setPriority(PRIORITY_VALUE)
                .setChannelId(CHANNEL_ID)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancelAll();
        currentNotificationID = NotificationID.getUniqueID();
        notificationManager.notify(currentNotificationID, builder.build());
        vibrate(context);
    }

    public static void toastReservationSuccess(Context context) {
        CharSequence text = "Reservation is made successfully!";
        Toast toast = Toast.makeText(context, text, toastDuration);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
        vibrate(context);
    }

    public static void toastReadySuccess(Context context, Reservation reservation) {
        String text = String.format("%s is notified!", reservation.getGuestName());
        Toast toast = Toast.makeText(context, text, toastDuration);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
        vibrate(context);
    }

    public static void toastTicketCancelled(Context context, Reservation reservation) {
        String text = String.format("Ticket #%d is cancelled", reservation.getTicketNumber());
        Toast toast = Toast.makeText(context, text, toastDuration);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
        vibrate(context);
    }

    public static void toastTicketCompleted(Context context, Reservation reservation) {
        String text = String.format("Ticket #%d is completed", reservation.getTicketNumber());
        Toast toast = Toast.makeText(context, text, toastDuration);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
        vibrate(context);
    }

    public static void toastPermissionDenied(Context context, String permission_name) {
        String text = String.format("Permission to acccess to " + permission_name + " was denied");
        Toast toast = Toast.makeText(context, text, toastDuration);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    public static void clearNotification(Context context) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancelAll();
    }



    private static void vibrate(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(1000);
        }
    }

    private static void makeSound(Context context) {
        final ToneGenerator toneGenerator[] = new ToneGenerator[1];
        try {
            if (toneGenerator[0] == null) {
                toneGenerator[0] = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
            }
            toneGenerator[0].startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 750);
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (toneGenerator[0] != null) {
                        toneGenerator[0].release();
                        Log.d(TAG, "ToneGenerator released");
                        toneGenerator[0]= null;
                    }
                }

            }, 750);
        } catch (Exception e) {
            Log.d(TAG, "Exception while playing sound:", e);
        }
    }
}
