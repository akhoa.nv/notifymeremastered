package com.nguyenrepo.notifyme.Java;

public class Reservation {
    private Long ticketNumber;
    private String guestId;
    private String guestName;
    private String guestEmail;
    private String guestPhone;
    private String hostName;
    private String hostEmail;
    private String hostPhone;
    private String hostAddress;
    private String hostId;
    private String status;

    public Reservation() {
        this.status = "not_ready";
    }

    public Reservation(Long ticketNumber, String guestId, String guestName, String guestEmail, String guestPhone,
                       String hostName, String hostEmail, String hostPhone, String hostAddress,
                       String hostId) {
        this();
        this.ticketNumber = ticketNumber;
        this.guestId = guestId;
        this.guestName = guestName;
        this.guestEmail = guestEmail;
        this.guestPhone = guestPhone;
        this.hostName = hostName;
        this.hostEmail = hostEmail;
        this.hostPhone = hostPhone;
        this.hostAddress = hostAddress;
        this.hostId = hostId;

    }

    public Long getTicketNumber() {
        return ticketNumber;
    }

    public String getGuestId() {
        return guestId;
    }

    public String getGuestName() {
        return guestName;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public String getGuestPhone() {
        return guestPhone;
    }

    public String getHostName() {
        return hostName;
    }

    public String getHostEmail() {
        return hostEmail;
    }

    public String getHostPhone() {
        return hostPhone;
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public String getHostId() {
        return hostId;
    }

    public String getStatus() {
        return status;
    }

    public void setTicketNumber(Long ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public void setGuestId(String guestId) {
        this.guestId = guestId;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public void setGuestEmail(String guestEmail) {
        this.guestEmail = guestEmail;
    }

    public void setGuestPhone(String guestPhone) {
        this.guestPhone = guestPhone;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public void setHostEmail(String hostEmail) {
        this.hostEmail = hostEmail;
    }

    public void setHostPhone(String hostPhone) {
        this.hostPhone = hostPhone;
    }

    public void setHostAddress(String hostAddress) {
        this.hostAddress = hostAddress;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }
}
