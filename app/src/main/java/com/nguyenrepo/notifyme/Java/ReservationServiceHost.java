package com.nguyenrepo.notifyme.Java;

import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ReservationServiceHost {
    public static final String PATH_USER = "users";
    public static final String PATH_RESTAURANTS = "restaurants";
    public static final String PATH_RESTAURANT_DB = "restaurant_db";
    private static final String TAG = "ReservationServiceHost";

    final Host[] m_myself = new Host[1];
    final Long[] m_ticketNumber = new Long[1];
    final List<Reservation> m_activeReservations = new ArrayList<>();
    private static List<ActiveReservationSubscriber> subscribers = new ArrayList<>();

    private static volatile ReservationServiceHost m_singleton = new ReservationServiceHost();

    public static void subscribe(ActiveReservationSubscriber s) {
        subscribers.add(s);
    }

    public static void unsubscribe(ActiveReservationSubscriber s) {
        subscribers.remove(s);
    }

    private void update() {
        for (int i = 0; i < subscribers.size(); i++) {
            subscribers.get(i).updateActiveReservation(m_activeReservations);
        }
    }
    
    private ReservationServiceHost() {}

    private void initializeUserInfo() {
        String userId = FirebaseAuth.getInstance().getUid();
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        FirebaseService.readUserInfo(userId, new FirebaseService.CallbackInterface() {
            @Override
            public void callback(DataSnapshot dataSnapshot) {
                m_myself[0] = dataSnapshot.getValue(Host.class);
                Log.d(TAG, "Get user's own information successfully!");
                //Get Current Ticket number from Firebase
                //TODO: Should use watch here (Concurrency issue)
                FirebaseService.readRestaurantTicketCounter(m_myself[0].getRestaurantId(),
                        new FirebaseService.CallbackInterface() {
                    @Override
                    public void callback(DataSnapshot dataSnapshot) {
                        m_ticketNumber[0] = (Long) dataSnapshot.getValue();
                    }
                });
                //Synchronize Reservation List and Set up listener for existing reservations
                FirebaseService.watchActiveReservations(m_myself[0].getRestaurantId(),
                        new FirebaseService.CallbackInterface() {
                    @Override
                    public void callback(DataSnapshot dataSnapshot) {
                        onChange_ActiveReservations(dataSnapshot);
                    }
                });
                update();
            }
        });
    }

    public void makeReservation(final String guestName, final String guestEmail, final String guestPhone,
                                final String userId, final TextView text_result) {
        if (userId == null) {
            text_result.setText("No User Info Available!");
            return;
        }
        //Increase ticket number to prevent collision
        FirebaseService.readUserTicket(userId, new FirebaseService.CallbackInterface() {
            @Override
            public void callback(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    Log.e(TAG, "Duplicate reservation! The guest already have one pending reservation.");
                    return;
                }
                FirebaseService.writeTicketCounter(m_myself[0].getRestaurantId(),
                        new Long(m_ticketNumber[0] + 1));
                m_ticketNumber[0]++;
                //Make reservation
                Reservation reservation = new Reservation();
                reservation.setTicketNumber(m_ticketNumber[0]);
                reservation.setGuestId(userId);
                reservation.setGuestName(guestName);
                reservation.setGuestEmail(guestEmail);
                reservation.setGuestPhone(guestPhone);
                reservation.setHostName(m_myself[0].getRestaurantName());
                reservation.setHostEmail(m_myself[0].getEmail());
                reservation.setHostPhone(m_myself[0].getPhoneNumber());
                reservation.setHostAddress(m_myself[0].getRestaurantAddress());
                reservation.setHostId(m_myself[0].getRestaurantId());
                //Commit reservation to Database
                FirebaseService.writeReservationToFirebase(m_myself[0].getRestaurantId(), userId, reservation);
            }
        });
    }

    public void makeReady(final String userId) {
        FirebaseService.readUserTicket(userId, new FirebaseService.CallbackInterface() {
            @Override
            public void callback(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null)
                    return;
                //Get ticket number
                Reservation reservation = dataSnapshot.getValue(Reservation.class);
                String ticketNumber = Long.toString(reservation.getTicketNumber());
                //Mark ticket as ready
                FirebaseService.writeReadyStatusToReservation(m_myself[0].getRestaurantId(), userId, reservation);
            }
        });
    }

    public void delete(final String userId) {
        FirebaseService.readUserTicket(userId, new FirebaseService.CallbackInterface() {
            @Override
            public void callback(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null)
                    return;
                //Get ticket number
                Reservation reservation = dataSnapshot.getValue(Reservation.class);
                String ticketNumber = Long.toString(reservation.getTicketNumber());
                FirebaseService.deleteReservation(m_myself[0].getRestaurantId(), ticketNumber, userId);
            }
        });
    }

    public List<Reservation> getReservationList() {
        return m_activeReservations;
    }

    public void onChange_ActiveReservations(DataSnapshot dataSnapshot) {
        //Create callback first
        FirebaseService.CallbackInterface callbackInterface = new FirebaseService.CallbackInterface() {
            @Override
            public void callback(DataSnapshot dataSnapshot) {
                //TODO: Use if needed
            }
        };
        try {
            //Type indicator for reservation list
            GenericTypeIndicator<HashMap<String, Reservation>> t =
                    new GenericTypeIndicator<HashMap<String, Reservation>>() {
                    };
            //Get HashMap of reservation (Cannot get List since it is a HashMap in Firebase)
            Map<String, Reservation> reservationMap = dataSnapshot.child("active").getValue(t);

            if (reservationMap == null) {
                m_activeReservations.clear();
                FirebaseService.destroyGuestListeners();
                update();
                return;
            }
            //Sort by reservation number
            reservationMap = new TreeMap<>(reservationMap);
            //Update active reservations
            m_activeReservations.clear();
            FirebaseService.destroyGuestListeners();
            for (String ticketNumber : reservationMap.keySet()) {
                Reservation currentReservation = reservationMap.get(ticketNumber);
                //Add reservation to local list
                m_activeReservations.add(currentReservation);
                FirebaseService.watchGuestReservation(currentReservation.getGuestId(), callbackInterface);
            }
        } catch (DatabaseException e) {
            GenericTypeIndicator<ArrayList<Reservation>> t =
                    new GenericTypeIndicator<ArrayList<Reservation>>() {
                    };
            ArrayList<Reservation> reservations = dataSnapshot.child("active").getValue(t);
            m_activeReservations.clear();
            FirebaseService.destroyGuestListeners();
            for (Reservation currentReservation : reservations) {
                //Add reservation to local list
                //TODO: Bandaid only
                if (currentReservation == null)
                    continue;
                m_activeReservations.add(currentReservation);
                FirebaseService.watchGuestReservation(currentReservation.getGuestId(), callbackInterface);
            }
        }
        update();
    }

    public static ReservationServiceHost getInstance() {
        m_singleton.initializeUserInfo();
        return m_singleton;
    }
}
