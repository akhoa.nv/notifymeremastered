package com.nguyenrepo.notifyme.Java;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.nguyenrepo.notifyme.Activities.SignInActivity;
import com.nguyenrepo.notifyme.R;

import java.util.List;

public class ReservationSlideAdapter extends PagerAdapter implements ActiveReservationSubscriber {
    private Context context = SignInActivity.getContext();
    private LayoutInflater inflater;
    private Activity activity;
    private View v;
    private List<Reservation> m_activeReservations = ReservationServiceHost.getInstance().getReservationList();
    private ReservationServiceHost reservationServiceHost = ReservationServiceHost.getInstance();
    private MyAdapterCallback callback;

    public ReservationSlideAdapter(Activity activity, View v, MyAdapterCallback callback){
        this.activity = activity;
        this.v = v;
        this.callback = callback;
        ReservationServiceHost.subscribe(this);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return m_activeReservations.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return ( view ==(ConstraintLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object){
        container.removeView((ConstraintLayout) object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position){
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.fragment_reservation, container, false);
        final Reservation reservation = m_activeReservations.get(position);

        ConstraintLayout reservationlayout = view.findViewById(R.id.reservation_layout);
        TextView reservationName = view.findViewById(R.id.reservation_name);
        TextView reservationTicketNumber = view.findViewById(R.id.reservation_ticket_num);
        final Button reservationReady = view.findViewById(R.id.reservation_ready);
        final Button reservationCancel = view.findViewById(R.id.reservation_cancel);

        if (position % 2 == 0) {
            reservationlayout.setBackgroundColor(0xFF000000);
            reservationReady.setBackgroundColor(0xFFFFFFFF);
            reservationCancel.setBackgroundColor(0xFFFFFFFF);

            reservationName.setTextColor(0xFFFFFFFF);
            reservationTicketNumber.setTextColor(0xFFFFFFFF);
            reservationReady.setTextColor(0xFF000000);
            reservationCancel.setTextColor(0xFF000000);
        } else {
            reservationlayout.setBackgroundColor(0xFFFFFFFF);
            reservationReady.setBackgroundColor(0xFF000000);
            reservationCancel.setBackgroundColor(0xFF000000);

            reservationName.setTextColor(0xFF000000);
            reservationTicketNumber.setTextColor(0xFF000000);
            reservationReady.setTextColor(0xFFFFFFFF);
            reservationCancel.setTextColor(0xFFFFFFFF);
        }

        reservationName.setText(m_activeReservations.get(position).getGuestName());
        reservationTicketNumber.setText("#" + m_activeReservations.get(position).getTicketNumber());

        final View.OnClickListener checkinListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.checkIn();
            }
        };

        View.OnClickListener readyListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reservationServiceHost.makeReady(reservation.getGuestId());
                NotificationService.toastReadySuccess(context, reservation);

                reservationReady.setText("Check-in");
                reservationReady.setOnClickListener(checkinListener);
            }
        };

        reservationCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: Implement cancel reservation function for cancelbtn
                reservationServiceHost.delete(reservation.getGuestId());
                NotificationService.toastTicketCancelled(context, reservation);

            }
        });

        if (m_activeReservations.get(position).getStatus() == "not_ready") {
        //    reservationCheckIn.setVisibility(View.INVISIBLE);
        //    reservationReady.setVisibility(View.VISIBLE);
            reservationReady.setOnClickListener(readyListener);
        } else if (m_activeReservations.get(position).getStatus() == "ready") {
        //    reservationCheckIn.setVisibility(View.VISIBLE);
        //    reservationReady.setVisibility(View.INVISIBLE);
            reservationReady.setText("Check-in");
            reservationReady.setOnClickListener(checkinListener);
        }

        container.addView(view);

        return view;
    }

    public void updateActiveReservation(List<Reservation> list) {
        m_activeReservations = list;
        notifyDataSetChanged();
    }

    public interface MyAdapterCallback {
        void checkIn();
    }
}
