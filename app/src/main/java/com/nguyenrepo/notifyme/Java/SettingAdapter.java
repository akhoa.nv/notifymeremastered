package com.nguyenrepo.notifyme.Java;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nguyenrepo.notifyme.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SettingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<String> mHeader;
    private List<Integer> mIcon;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    public SettingAdapter(Context context, List<String> header, List<Integer> icon) {
        this.mInflater = LayoutInflater.from(context);
        this.mHeader = header;
        this.mIcon = icon;
        this.context = context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;
        ImageView image;

        ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.setting_header);
            image = itemView.findViewById(R.id.setting_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.fragment_setting, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((TextView)holder.itemView.findViewById(R.id.setting_header)).setText(mHeader.get(position));
        ((ImageView)holder.itemView.findViewById(R.id.setting_icon)).setImageDrawable(context.getResources().getDrawable(mIcon.get(position)));
    }

    @Override
    public int getItemCount() {
        return mHeader.size();
    }

    String getItem(int id) {
        return mHeader.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
