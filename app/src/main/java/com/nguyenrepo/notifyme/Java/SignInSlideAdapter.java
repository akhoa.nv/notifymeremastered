package com.nguyenrepo.notifyme.Java;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.nguyenrepo.notifyme.Activities.SignInActivity;
import com.nguyenrepo.notifyme.Activities.SignUpGuestActivity;
import com.nguyenrepo.notifyme.Activities.SignUpHostActivity;
import com.nguyenrepo.notifyme.R;

public class SignInSlideAdapter extends PagerAdapter {
    private Context context = SignInActivity.getContext();
    private LayoutInflater inflater;
    private Activity activity;

    public SignInSlideAdapter(Activity activity){
        this.activity = activity;
    }

    private int[] images = {
            R.drawable.client,
            R.drawable.host
    };

    private String[] headers = {
            context.getResources().getString(R.string.sign_up_guest_header),
            context.getResources().getString(R.string.sign_up_host_header)
    };

    private String[] body_des = {
            context.getResources().getString(R.string.sign_up_guest_des),
            context.getResources().getString(R.string.sign_up_host_des)
    };

    private String[] btn_text = {
            context.getResources().getString(R.string.sign_up_guest_btn),
            context.getResources().getString(R.string.sign_up_host_btn)
    };

    private int[] bg_color = {
            Color.rgb(137,233,217),
            Color.rgb(253,108,85)
    };

    @Override
    public int getCount() {
        return headers.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return ( view ==(ConstraintLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object){
        container.removeView((ConstraintLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position){
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.fragment_signup, container, false);

        ConstraintLayout signuplayout = view.findViewById(R.id.sign_up_layout);
        ImageView img = (ImageView) view.findViewById(R.id.sign_up_img);
        TextView header = (TextView) view.findViewById(R.id.sign_up_header);
        TextView des = (TextView) view.findViewById(R.id.sign_up_des);
        Button btn = (Button) view.findViewById(R.id.sign_up_btn);

        signuplayout.setBackgroundColor(bg_color[position]);
        img.setImageResource(images[position]);
        header.setText(headers[position]);
        des.setText(body_des[position]);
        btn.setText(btn_text[position]);

        if (position == 0) {
            btn.setBackgroundColor(bg_color[1]);
        } else {
            btn.setBackgroundColor(bg_color[0]);
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    activity.startActivityForResult(new Intent(context, SignUpGuestActivity.class), 0);
                } else {
                    activity.startActivityForResult(new Intent(context, SignUpHostActivity.class), 0);
                }
            }
        });

        container.addView(view);
        return view;
    }
}
